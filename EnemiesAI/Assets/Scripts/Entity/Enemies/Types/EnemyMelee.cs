using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
///     We're making this class act like an container with all the variables we need to make the melee enemy functional.
/// We could separete the variables here, but on the editor we would have to change multiples components, right now
/// we setup everything here and just pass to waht component we need on startup.
/// </summary>

[RequireComponent(typeof(PatrolAreaState))]
[RequireComponent(typeof(FollowTargetState))]
[RequireComponent(typeof(MeleeAttackState))]

public class EnemyMelee : EnemyBase
{
    #region Public Fields / Properties
    [Header("States Conditions")]
    
    [Header("Patrol")]
    [Tooltip("The points we're going to patrol.")]
    public List<Transform> PatrolPoints;

    [Tooltip("The time we wait when we reach an point and go to the next one.")]
    public float TimeBetweenPoints = 1f;
        
    [Header("Following")]
    [Tooltip("The distance to start following the target.")]
    public float DistanceToStartFollowing = 10f;

    [Tooltip("The actual transform that the enemy will follow and check for distance.")]
    public Transform TargetTransform;
    
    [Header("Melee Attack")]
    [Tooltip("The distance to start attacking.")]
    public float DistanceToStartAttacking = 2f;

    [Tooltip("The time we wait between each attack.")]
    public float TimeBetweenAttacks = 2f;

    [Tooltip("The amount of base damage we're going to apply when we perform the attack.")]
    public float DamageAmount = 10f;
    #endregion

    #region Private fields
    private PatrolAreaState _patrolState;
    private FollowTargetState _followTargetState;
    private MeleeAttackState _meleeAttackState;
    #endregion

    #region Mono
    protected override void Awake()
    {
        ////We cache components here.
        _patrolState = GetComponent<PatrolAreaState>();
        _followTargetState = GetComponent<FollowTargetState>();
        _meleeAttackState = GetComponent<MeleeAttackState>();

        ////We call for initializers here.
        ////We should check for null instances just to avoid errors.
        _patrolState.StateAwake();
        _followTargetState.StateAwake();
        _meleeAttackState.StateAwake();
    }

    protected override void Start()
    {
        base.Start();

        //We deal with variables here.
        _patrolState.PatrolPoints = PatrolPoints;   //Our points list.
        _patrolState.TimeBetweenPoints = TimeBetweenPoints;     //Our wait timer between points.

        _followTargetState.StopFromDestination = DistanceToStartAttacking;  //The distance where we don't update our destination anymore.
        _followTargetState.TargetTransform = TargetTransform; //Our Target to follow.


        //We call for initializers here.
        _patrolState.StateStart();
        _followTargetState.StateStart();
        _meleeAttackState.StateStart();

        //This is to define the initial state to not be in void state.
        EnemyChangeState(EnemyStates.Patrol, _patrolState);
        
        MyEnemyType = EnemyTypes.Melee;
    }

    protected override void Update()
    {
        base.Update();
        
        StatesConditionsControl();
    }
    #endregion

    /// <summary>
    /// This is the "brain" of our enemy, the pseudo-code for the logic is this:
    /// Pseudo logic:
    ///     If the distance between enemy and player is higher than DistanceToStartFollowing we keep on patrol state.
    ///     If the distance between enemy and player is lower than DistanceToStartFollowing we go to follow state.
    ///     If the distance between enemy and player is lower than DistanceToStartAttacking we go to attack state.
    ///     If we're performing an animation attack we don't change the current state if we're trying to leave melee state. 
    /// </summary>
    protected override void StatesConditionsControl()
    {
        if (_patrolState == null || _meleeAttackState == null || _followTargetState == null)    //If any of our states are null we don't do nothing.
            return;

        //We cache this here to reuse on everything we need to check for distance.
        float myDistance = Vector3.Distance(this.gameObject.transform.position, TargetTransform.position);  //This is the distance from the target.
        
        //TODO: We should check if we are inside the attack animation.
        if (_meleeAttackState.PerformingAttack)
        {
            if (myDistance > DistanceToStartFollowing && myDistance > DistanceToStartAttacking)  //If the distance it's higher than 
            {
                if (MyEnemyStates != EnemyStates.Patrol)    //If we arent running the patrol state we change it, if we are we don't do anything.
                {
                    EnemyChangeState(EnemyStates.Patrol, _patrolState);
                }
            }
            else if (myDistance <= DistanceToStartFollowing && myDistance > DistanceToStartAttacking)
            {
                if (MyEnemyStates != EnemyStates.FollowTarget)
                {
                    EnemyChangeState(EnemyStates.FollowTarget, _followTargetState);
                }
            }
            else if (myDistance <= DistanceToStartAttacking)    //If we are close enough to attack we atyack
            {
                if (MyEnemyStates != EnemyStates.MeleeAttack)
                {
                    EnemyChangeState(EnemyStates.MeleeAttack, _meleeAttackState);   //We change our state here because we have internal control to not perform multiple attacks at once.
                }
            }
        }

    }

    protected override void EnemyDeath()
    {
        //TODO: On EnemyDeath we will move the enemy to a pool system for reuse.
    }

    public override void EnemyReceiveDamage()
    {
        Debug.Log("Enemy receiving damage!");
    }
    
}