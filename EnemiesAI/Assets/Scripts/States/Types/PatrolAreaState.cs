﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//We're requiring the nav mesh because this state will deal directly with movement.
[RequireComponent(typeof (NavMeshAgent))]
public class PatrolAreaState : StateBase
{
    /// <summary>
    ///      This allow us to have an better control
    /// over the entire state and where we're and what
    /// we're doing.
    /// </summary>
    private enum PatrolInternControl    
    {
        None = 0,
        Moving = 1,     //MeshAgent is moving.
        Waiting = 2,    //We are waiting for the timer before we go to moving. We're just looking around.
    }

    private PatrolInternControl _internControlState;

#region Public / Properties fields

    public List<Transform> PatrolPoints
    {
        get { return _patrolPoints; }
        set { _patrolPoints = value; }
    }

    public float TimeBetweenPoints
    {
        get { return _timeBetweenPoints; }
        set { _timeBetweenPoints = value; }
    }

#endregion

#region Private fields

    private List<Transform> _patrolPoints;      //This is our list of points to patrol between.
    private float _timeBetweenPoints = 1;       //The time we wait between points to look around.
    
    private NavMeshAgent _meshAgent;        //Our nav mesh agent.

    private int _patrolPointsCounter = 0;       //This is our internal counter to where in the list we are.
    private float _internalTimer = 0f;          //Timer for the waiting.
    private float _distanceToGoToNextPoint = 1.2f;      //The distance from the gameObject to the current point to change to Waiting.

#endregion

    public override void StateAwake()
    {
        base.StateAwake();

        _meshAgent = GetComponent<NavMeshAgent>();
    }

    public override void StateStart()
    {
        if (PatrolPoints == null || PatrolPoints.Count <= 1)
            Debug.Log("We don'w have enough points for the PatrolState!", this.gameObject);

        _internControlState = PatrolInternControl.Moving;
    }

    /// <summary>
    ///     Our main Update code.
    ///     If we don't have any points to patrol we don't even do anything. We could show some Warning here, but
    /// we're already showing an warning on Start.
    ///     Here we run the condition too.
    /// </summary>
    public override void StateUpdate()
    {
        if (PatrolPoints == null)
            return;

        //Debug.Log("Patrol State: " + _internControlState);

        switch (_internControlState)
        {
            case PatrolInternControl.Moving:
                FollowPoints();
                break;

            case PatrolInternControl.Waiting:
                WaitForTimer();
                break;
        }

        ConditionControl();
    }
    
    public override void StateCooldown()
    {

    }

    /// <summary>
    ///     This is our main condition, if we are near the current patrol point
    /// we change our current state to Waiting.
    /// </summary>
    private void ConditionControl()
    {
        float myDistance = Vector3.Distance(this.gameObject.transform.position,
            PatrolPoints[_patrolPointsCounter].position);

        if (myDistance < _distanceToGoToNextPoint)                   //Check for the distance.
        {
            if (_internControlState != PatrolInternControl.Waiting)     //TODO: Why this is redundant?
            {
                _internControlState = PatrolInternControl.Waiting;
            }
        }
    }

    /// <summary>
    /// If we want to do something when we are moving the entity we can add
    /// code inside this method, we could play an particle or something.
    /// </summary>
    private void FollowPoints()
    {
        _meshAgent.SetDestination(PatrolPoints[_patrolPointsCounter].position);
    }

    /// <summary>
    ///     Simply timer wait to take a second when we reach the patrol point
    /// before we go to the next one.
    ///     Increase the current counter to move to the next one.
    ///     We have a simply check to make we stay inside our list counter.
    ///     We could change this to be a coroutine, so we can make this logic simply and 
    /// with less lines.
    ///     TODO:Need to smooth the look rotation after we wait the time and move to the next position.
    /// </summary>
    private void WaitForTimer()
    {
        _internalTimer += Time.deltaTime;

        //Debug.Log("We are waiting for the internal timer.");

        //We wait for the timer here.
        if (_internalTimer > TimeBetweenPoints)
        {
            //Debug.Log("We are increasing the current timer.");
            _patrolPointsCounter += 1;      //We increase our point counter.
            //Debug.Log("Current point: " + _patrolPointsCounter);

            if (_patrolPointsCounter > PatrolPoints.Count - 1)  //Make sure we stay inside or list counter.
                _patrolPointsCounter = 0;

            this.gameObject.transform.LookAt(PatrolPoints[_patrolPointsCounter]);   //TODO: Smooth this look rotation.
            
            _internalTimer = 0f;    //Reset the timer.

            _internControlState = PatrolInternControl.Moving;   //Change the current state.
        }
    }
    
    /// <summary>   
    /// This is just to have a better visualization of the
    /// patrol path, we can take this off from the final release
    /// if we want. 
    /// We could add some bool to check if we want to show a visualization or not.
    /// </summary>
    public void OnDrawGizmosSelected()
    {
        if (PatrolPoints.Count == 0 || PatrolPoints == null)    //If we don't have any point we go and find the patrol point.
        {
            PatrolPoints = GetComponent<EnemyMelee>().PatrolPoints;
        }
        
        //TODO: We could try to do some path validation, so we are drawing the real path and not just lines.
        //TODO: Maybe we can ask for a path here and see what we can do with it.
        for (int x = 0; x < PatrolPoints.Count; x++)    //We loop through each point in the list to draw some lines to become easy to see the patrol path.
        {
            Gizmos.color = Color.green;

            Gizmos.DrawSphere(PatrolPoints[x].position, 0.25f);

            Gizmos.color = Color.red;
            
            if (x == PatrolPoints.Count - 1)
                Gizmos.DrawLine(PatrolPoints[x].position, PatrolPoints[0].position);
            else
                Gizmos.DrawLine(PatrolPoints[x].position, PatrolPoints[x + 1].position);
        }
    }
}