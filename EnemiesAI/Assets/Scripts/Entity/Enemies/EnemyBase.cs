using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum EnemyStates
{
    None = 0,
    Waiting = 1,
    Patrol = 2,
    MeleeAttack = 3,
    RangedAttack = 4,
    Fleeing = 5,
    FollowTarget = 6,
    LookForTarget = 7,
}

public enum EnemyTypes
{
    None = 0,
    Melee = 1,
    Ranged = 2,
}

//TODO: Implement STATS for the enemy.

public class EnemyBase : EntityBase
{
    protected EnemyStates _enemyStates;
    public EnemyStates MyEnemyStates
    {
        get { return _enemyStates;}
        //We dont have a setter here because to change states we need to call EnemyChangeState!
    }

    public EnemyTypes MyEnemyType = EnemyTypes.None;
    
    protected override void Start()
    {
        MyEntityType = EntityType.Enemy;    //Update our entity type for future referencies if we need!
    }

    protected virtual void EnemyDeath() {}   //This can be kinda of a event!
    public virtual void EnemyReceiveDamage() {}   //We call this to apply damage.

    /// <summary>
    ///  EnemyStates is just for control based in the enum.
    ///  StateBase is used to feed methods to the EnemyAction and EnemyCooldown.
    ///     We could ask for two separetes methods here, but we problably will call StateEnter and StateExit.
    /// </summary>
    protected virtual void EnemyChangeState(EnemyStates state, StateBase stateAction)
    {
        //TODO: Implement StateEnter and StateExit methods and logic to allow for unique increments.
        Debug.Log("CHANGING ENEMY STATE: " + state);

        //TODO:We need to call this from the previous state!
        //TODO:We need some way to track the previous state, or we could add a switch here.
        //TODO:What if we add an delegate to state exit, so we add when we change the state, and check too.
        ////If we have a previous state, we call the StateEnter.
        //if (_enemyStates != EnemyStates.None)
        //{
        //    stateAction.StateEnter();
        //}
        
        //We update the state enum.
        _enemyStates = state;
        
        //We update our action delegate.
        MyEntityAction = null;
        MyEntityAction += stateAction.StateUpdate;

        //We update our cooldown delegate.
        MyEntityCooldown = stateAction.SubscribeCooldown(MyEntityCooldown);

        //We call the state enter.
        stateAction.StateEnter();
    }

    /// <summary>
    /// We use this to control states conditions, this allow for cleaner code.
    /// This needs to be called inside our update to check for conditions every frame
    /// and make deisions on it's own.
    /// </summary>
    protected virtual void StatesConditionsControl() { }

    //If we can make a generic implementation we can use this, otherwise its not really useful I think.
    protected List<StateBase> GetStateComponents()  
    {
        List<StateBase> components = GetComponents<StateBase>().ToList();

        return components;
    }
}