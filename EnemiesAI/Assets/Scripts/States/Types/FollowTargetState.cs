using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class FollowTargetState : StateBase
{
    
#region Public / Properties fields

    public Transform TargetTransform
    {
        get { return _targetTransform; }
        set { _targetTransform = value; }
    }
    
    public float StopFromDestination
    {
        get { return _stopFromDestination;}
        set { _stopFromDestination = value;}
    }

#endregion

#region Private fields

    private Transform _targetTransform;     //This is oour target transform.    
    private float _stopFromDestination;     //When we should stop moving and change state. This is just to make sure we don't overreact and go inside our target.

    private NavMeshAgent _meshAgent;        //Our nav mesh agent.

#endregion

    /// <summary>
    /// This is called on the Awake inside the enemu type.
    /// We do basic cache for components too.
    /// </summary>
    public override void StateAwake()
    {
        //If we don't have a target, we will go for the player.
        if (TargetTransform == null)
            TargetTransform = GameObject.FindGameObjectWithTag(TagHelper.PlayerTag).GetComponent<Transform>();

        _meshAgent = GetComponent<NavMeshAgent>();
    }

    /// <summary>
    /// We deal with variables and for any error we could have.
    /// </summary>
    public override void StateStart()
    {
        if (TargetTransform == null)
            Debug.Log("We don't have a target for the FolloTargetState! And the Find have returned null. CHECK FOR TAGS!", this.gameObject);

        StopFromDestination = StopFromDestination + this.gameObject.transform.localScale.x; //This makes sure we get in consideration the size of the gameObject when we calculate the distance.

        Debug.Log(StopFromDestination);
    }

    /// <summary>
    /// If we don't have a TargetTransform or a NavMeshAgent we don't do anything, we should
    /// throw a error(TODO).
    /// If we're close enough from the target we don't have to update the target too, the enemy state will
    /// probably going to change to melee attack too.
    /// Otherwise, we just update the destination for the NavMeshAgent.
    /// </summary>
    public override void StateUpdate()
    {
        //If we don't have this stuff we can't do anything. We should throw a error.
        if (TargetTransform == null || _meshAgent == null)
            return;

        //If we're near the target we don't do anything and probably will move to the attack state.
        if (Vector3.Distance(this.gameObject.transform.position, TargetTransform.position) < _stopFromDestination)
        {
            if (_meshAgent.velocity != Vector3.zero)    //We need to stop the agent too! TODO: Check inside the enemy attack too so we don't move there.
                _meshAgent.Stop();

            return;
        }

        //Debug.Log("Distance from target: " + Vector3.Distance(this.gameObject.transform.position, TargetTransform.position));
        Debug.DrawLine(this.gameObject.transform.position, TargetTransform.position, Color.red);
        
        _meshAgent.Resume();

        _meshAgent.SetDestination(TargetTransform.position);    //Refresh the destination.
    }

    public override void StateCooldown()
    {
        base.StateCooldown();
    }
}