using UnityEngine;
using System.Collections;

//TODO: We are doing two types of attacks, an fast one that deals a little damage but the player can avoid and a 
//TODO: slower one that can be avoided if the player pays enough attention, this one will deal more damage.

//Slow attack will have priority over fast attack, so every time we can slow attack we will perform the slow attack.
//And when the slow attack its on cooldown we will perform the melee attack.

public class MeleeAttackState : StateBase
{

#region Public / Properties fields

    public float MaxDistanceToApplyDamage
    {
        get { return _maxDistanceToApplyDamage; }
        set { _maxDistanceToApplyDamage = value; }
    }

    public float DamageAmount
    {
        get { return _damageAmount; }
        set { _damageAmount = value; }
    }

    public bool CanFastAttack
    {
        get { return _canFastAttack; }
    }

    public bool CanSlowAttack
    {
        get { return _canSlowAttack; }
    }

    public float FastAttackCooldown
    {
        get { return _fastAttackCooldown; }
        set { _fastAttackCooldown = value; }
    }

    public float SlowAttackCooldown
    {
        get { return _slowAttackCooldown; }
        set { _slowAttackCooldown = value; }
    }

    public float FastAttackCooldownRatio
    {
        get { return _fastAttackCooldownRatio; }
        set { _fastAttackCooldownRatio = value; }
    }

    public float SlowAttackCooldownRatio
    {
        get { return _slowAttackCooldownRatio; }
        set { _slowAttackCooldownRatio = value; }
    }

    public Transform TargetTransform
    {
        get { return TargetTransform; }
        set { _targetTransform = value; }
    }

    public bool PerformingAttack
    {
        get { return _performingAttack;}
    }

    public bool HaveFastAttack;
    public bool HaveSlowAttack;

#endregion

#region Private fields

    private float _damageAmount = 10f;              //The amount of damage we apply to the target.
    private float _maxDistanceToApplyDamage = 2f;   //The max distace from the target that will be applied the damage.
    private float _fastAttackCooldown = 3f;             //The time between each fast attack.
    private float _fastAttackCooldownRatio = 1f;        //The ratio based on the delta time we increase our cooldown timer, 1 its unefected;
    private float _slowAttackCooldown = 6f;             //The time between each slow attack.
    private float _slowAttackCooldownRatio = 1f;        //The ratio based on the delta time we increase our cooldown timer.

    private Transform _targetTransform;         //The target we will use to check for distance and direction to apply or not damage.

    private bool _canFastAttack = true;                 //If we can fast attack or not.
    private bool _canSlowAttack = true;                 //If we can slow attack or not.
    private bool _performingAttack = false;         //If we are in the mid of the attack animation.

    private float _cooldownFastTimer = 0f;              //The internal timer we use to increase by delta time.
    private float _cooldownSlowTimer = 0f;
    
    private float _fastAttackDamage;
    private float _slowAttackDamage;

    /// <summary>
    /// This ratio is based on the BaseDamage, we get the final result multipliying the base damage
    /// by our ratio.
    /// </summary>
    //TODO:We could move this to be public to allow for better tweaking. We are serializing this just to show on the editor.
    [SerializeField] private float _fastAttackRatio = 0.5f;
    [SerializeField] private float _slowAttackRatio = 1.2f;
    
#endregion

    public override void StateAwake()
    {

    }

    public override void StateStart()
    {
        //We can affect the damage based on player level too! Everytime we enable the gameObject we check for the level and recalculate it.
        _fastAttackDamage = _damageAmount * _fastAttackRatio;
        _slowAttackDamage = _damageAmount * _slowAttackRatio;

        if (TargetTransform == null)        //Fail check to make sure we have the player as an transform.
            TargetTransform = GameObject.FindGameObjectWithTag(TagHelper.PlayerTag).GetComponent<Transform>();
    }

    /// <summary>
    /// Our main logic to perform attack.
    /// The priority it's to perform slow attacks, when its on cooldown we perform fast attack to fill the gap.
    /// Here we're basically triggering animations, we are going to try to make the 
    /// animation itself call for the method we want when we need to check for the 
    /// distance and field of view.
    /// </summary>
    public override void StateUpdate()
    {
        if (PerformingAttack)   //If we are inside an attack animation we don't trigger another attack.
            return;
        
        //if (CanSlowAttack)
        //{
        //    PerformSlowAttak();
        //}
        //else if (CanFastAttack)
        //{
        //    PerformFastAttack();
        //}
    }

    /// <summary>
    /// If we can slow/fast attack we don't need to handle the attack cooldown, so we just return.
    /// </summary>
    public override void StateCooldown()
    {
        if (CanFastAttack && CanSlowAttack)
            return;

        HandleFastAttackCooldown();
        HandleSlowAttackCooldown();
    }


#region Private methods

    /// <summary>
    /// We handle the cooldown timer here.
    /// We increase the timer based on the delta time and we multiply
    /// it by a ratio, ratio 1 it will not affect the timer, but a negative
    /// or a positive will affect the final result when we add to the _cooldownTimer.
    /// </summary>
    private void HandleFastAttackCooldown()
    {
        _cooldownFastTimer += Time.deltaTime * _fastAttackRatio;

        if (_cooldownFastTimer > _fastAttackCooldown)
        {
            _canFastAttack = true;
        }
    }

    //TODO: Refactor this(?)
    private void HandleSlowAttackCooldown()
    {
        _cooldownSlowTimer += Time.deltaTime * _slowAttackRatio;

        if (_cooldownSlowTimer > _slowAttackCooldown)
        {
            _canSlowAttack = true;
        }
    }
    
#endregion

#region Public methods

    /// <summary>
    /// We're going to call this from the animation.
    /// This is a simply check to see if the player is on the front off the enemy and
    /// it's near the enemy, if it is we apply damage to the player, if not we don't 
    /// do anything.
    /// </summary>
    public void CheckForPlayerDistance()
    {
        //Check for distance.

        //Do dot product to check if the player is in front.

        //Apply damage if we met requirements.
    }

#endregion
}