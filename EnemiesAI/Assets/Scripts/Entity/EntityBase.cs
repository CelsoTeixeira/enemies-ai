﻿using System;
using UnityEngine;
using System.Collections;

public enum EntityType
{
    None = 0,
    NPC = 1,
    Enemy = 2
}

//We could make the player inherit from this too.
public class EntityBase : MonoBehaviour 
{
    protected EntityType MyEntityType = EntityType.None;

    protected Action MyEntityAction;     //Main action update.
    protected Action MyEntityCooldown;   //Cooldown update.
    
    protected virtual void Awake() {}    //Cache components.
    protected virtual void Start() {}    //Deal with variables.

    protected virtual void Update()
    {
        //MyEntityAction?.Invoke(); can't use null propagation on unity yet.

        if (MyEntityAction != null)
        {
            MyEntityAction();
        }

        if (MyEntityCooldown != null)
        {
            MyEntityCooldown();
        }
    }   
}