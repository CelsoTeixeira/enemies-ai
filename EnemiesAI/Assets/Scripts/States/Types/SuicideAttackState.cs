﻿using UnityEngine;
using System.Collections;

public class SuicideAttackState : StateBase
{

#region Public / Properties fields

    public float BaseDamage 
    {
        get { return _baseDamage; }
        set { _baseDamage = value; }
    }

    public float DamageScale 
    {
        get { return _damageScale; }
        set { _damageScale = value; }   //TODO: We probably want to do some validation here later after we test some values.
                                        //We could call this on OnEnable and do something with player level ti scale better? 
    }

    public float MinimunDamage          //We probably wan't to call this on OnEnable to do some validation.
    {
        get { return _minimunDamage; }
        set { _minimunDamage = value; }
    }

    public float MaximunDamage
    {
        get { return _maximunDamage; }
        set { _maximunDamage = value; }
    }

    public float ExplosionRadious
    {
        get { return _explosionRadious; }
        set { _explosionRadious = value; }    //TODO:We want some validation here too.
    }

    public float ExplosionTimer
    {
        get { return _explosionTimer; }
        set { _explosionTimer = value; }        //TODO: We want some validation here, this can't be less than zero.
    }

#endregion

#region Private fields

    private float _baseDamage = 10f;        //This is the base for all the damage calculations.
    private float _damageScale = 1f;        //The base scale we scale the damage when we apply damage.
    private float _minimunDamage;           //The minumun damage we apply to any entity on the explosion radious.
    private float _maximunDamage;           //The maximun damage we apply to any entity on the explosion radious.
    private float _timeToExplode = 3f;      //The time we wait before we explode.

    private float _explosionRadious = 4f;   //The radious we will apply the damage.

    [SerializeField] private int _layerToCheck;     //We need to bit shift this.

    private bool _beginExplosion = false;       //We use this to check if we already have started the explosion.

    private float _explosionTimer;


#endregion

    public override void StateUpdate()
    {
        if (!_beginExplosion)
            return;

        //TODO: We could do some particles here to show some feedback when we start to prepare to explode.

        //We probably want the nav mesh agent and make it stop if it's moving.

        _explosionTimer += Time.deltaTime;

        if (_explosionTimer >= _timeToExplode)
        {
            DoExplosion();
        }

    }

    public override void StateCooldown()
    {
        //We probably don't need anything here. Just check later.
    }

#region Private methods

    private void DoExplosion()
    {
        //Play particles.

        //Do a sphere cast.

        //Check for entitys inside it.

        //Apply damage based on distance.

        //Disable on send to enemy pool.

        //TODO: Remenber to reset explosion timer and begin explosion.
    }

#endregion

}