using UnityEngine;
using System.Collections;

/// <summary>
/// I think we don't need this to be MonoBehaviour because this is just a base
/// for the slow and fast attack, this is just to not duplicate code.
/// </summary>
public class MeleeAttackAction
{
    #region Public / Properties

    public float Damage
    {
        get { return _damage; }
        set
        {
            if (value > 0)
                _damage = value;
            else
                _damage = 1f;
        }
    }

    public float MaxDistanceToApplyDamage
    {
        get { return _maxDistanceToApplyDamage; }
        set
        {
            if (value > 0)
                _maxDistanceToApplyDamage = value;
            else
                _maxDistanceToApplyDamage = 1f;     //1f it's our default value.
        }
    }

    public bool CanAttack
    {
        get { return _canAttack;}
    }

    public Animator MyAnimator
    {
        get { return _animator;}
        set { _animator = value; }
    }

    #endregion

    #region Private

    protected float _damage;                    //How much damage we will apply when we perform the attack;
    protected float _maxDistanceToApplyDamage;  //The max distance we can reach.
    protected bool _canAttack;                 //Can we perform the attack?

    protected Animator _animator;

    #endregion

    #region Methods

    public bool AskForAttack()
    {
        if (_canAttack)
        {
            PerformAttack();
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// We will use this to trigger the animations.
    /// The actual logic to check for the player distance and 
    /// if its on front will be called from the animation I think.
    /// I will test this method, if I like it we will apply it, but we're
    /// going to do another one too based on colliders and see what it's 
    /// faster and simply.
    /// </summary>
    protected virtual void PerformAttack()
    {
        //Look at the target.

        //Play animation

        //Check for player position when we perform the attack.

        //Deal damage according with the checking.
    }

    #endregion


}