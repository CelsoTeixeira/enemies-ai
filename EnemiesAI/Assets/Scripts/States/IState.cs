﻿using System;
using UnityEngine;
using System.Collections;

public interface IState
{
    void StateAwake();
    void StateStart();
    void StateUpdate();
    void StateCooldown();

    Action SubscribeCooldown(Action cooldownDelegate);

    void StateEnter();
    void StateExit();
}