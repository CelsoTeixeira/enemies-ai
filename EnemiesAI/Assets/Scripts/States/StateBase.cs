﻿using System;
using UnityEngine;
using System.Collections;

public class StateBase :MonoBehaviour, IState
{
    
    //TODO: Clean DEBUG!
    public virtual void StateAwake()
    {
        Debug.Log("State Awake");
    }

    public virtual void StateStart()
    {
        Debug.Log("State Start");
    }

    public virtual void StateUpdate()
    {
        Debug.Log("State Update");
    }

    public virtual void StateCooldown()
    {
        Debug.Log("State Cooldown");
    }
    
    public virtual void StateEnter()
    {
        Debug.Log("Entering State");
    }

    public virtual void StateExit()
    {
        Debug.Log("Leaving State");
    }

    public virtual void OnEnable()
    {
        Debug.Log("Enabling the object'");
    }

    //This is to make sure we add the cooldown method only once in the Cooldown Action.
    private bool _cooldownAdded = false;
    
    public virtual Action SubscribeCooldown(Action cooldownDelegate)
    {
        //This is to make sure we only subscribe the cooldown method once
        if (this._cooldownAdded)
            return cooldownDelegate;

        cooldownDelegate += StateCooldown;  //We increase the parameter.
        this._cooldownAdded = true;

        return cooldownDelegate;    //we return the parameter with the addition.
    }

}